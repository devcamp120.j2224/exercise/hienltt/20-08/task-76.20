package com.devcamp.erdentity.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.erdentity.model.CCountry;
import com.devcamp.erdentity.repository.CountryRepository;

import org.springframework.data.domain.PageRequest;


import java.util.*;

@RestController
public class CountryController {
	@Autowired
	private CountryRepository countryRepository;

	@GetMapping("/regions/{countryCode}")
	public ResponseEntity<List<CCountry>> getRegionsByCountryCode(@PathVariable("countryCode") String countryCode) {
		try {
			int lenght = 3;
			int start = 0;
			List<CCountry> vCountry = countryRepository.findCountryByCountryNameLike(countryCode, PageRequest.of(start, lenght));

			if (vCountry != null) {
				return new ResponseEntity<>(vCountry, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

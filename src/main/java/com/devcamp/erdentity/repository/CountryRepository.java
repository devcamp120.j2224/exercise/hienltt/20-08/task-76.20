package com.devcamp.erdentity.repository;

import java.util.List;

import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.erdentity.model.CCountry;

@Repository
public interface CountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);
	
	@Query(value = "FROM #{#entityName} WHERE country_name like %?1%")
	List<CCountry> findCountryByCountryNameLike(String countryname, Pageable pageable);
	
	@Query(value = "SELECT * FROM p_country WHERE country_name like %:name1%", nativeQuery = true)
	List<CCountry> timCountryByCountryNameLike(@Param("name1") String countryname);
	
	@Query(value = "SELECT * FROM p_country WHERE country_name like :name1 ORDER BY country_name DESC", nativeQuery = true)
	List<CCountry> findCountryByCountryNameDesc(@Param("name1") String name1);

}
